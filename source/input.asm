	BITS 16

; Read Char
; Reads a character keypress
; AL—Returns the key pressed
salsos_read_char:
	mov ah, 0
	int 16h

	ret
