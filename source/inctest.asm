db 'This is an executable. Run it with run.', 0

jmp start

	BITS 16
	ORG 32768

	%INCLUDE "source/salsos.inc"

start:

mov si, text
call print
ret

text db 'Foo', NEWLINE, 'Bar', 0
