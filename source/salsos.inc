	%DEFINE NEWLINE 0dh, 0ah
	%DEFINE EXEC_CONFIRM 'This is an executable. Run it with run.', 0

print:
	pusha
	mov ah, 0Eh		; int 10h 'print char' function

.repeat:
	lodsb			; Get character from string
	cmp al, 0
	je .done		; If char is zero, end of string
	int 10h			; Otherwise, print it
	jmp .repeat

.done:
	popa
	ret

clear:
	; Clear the screen
	mov ah, 6			; Scroll full-screen
	mov al, 0			; Normal white on black
	mov bh, 7			;
	mov cx, 0			; Top-left
	mov dh, 24			; Bottom-right
	mov dl, 79
	int 10h

	; Put the cursor at the top left
	mov dx, 0
	call move_cursor

	ret

read_char:
	mov ah, 0
	int 16h

	ret

hide_cursor:
	pusha

	mov ch, 32
	mov ah, 1
	mov al, 3			; Must be video mode for buggy BIOSes!
	int 10h

	popa
	ret

show_cursor:
	pusha

	mov ch, 6
	mov cl, 7
	mov ah, 1
	mov al, 3
	int 10h

	popa
	ret

; DH—Row
; DL—Column
move_cursor:
	pusha

	mov bh, 0
	mov ah, 2
	int 10h

	popa
	ret

