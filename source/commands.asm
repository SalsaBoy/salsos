	BITS 16

; Check Command
; Checks and runs the command given
; SI—the command entered in the prompt
salsos_check_command:
	mov ax, si
	call salsos_uppercase_string

	mov di, command_screen_test_text
	call salsos_compare_strings
	jc salsos_command_screen_test

	mov di, command_show_version_text
	call salsos_compare_strings
	jc salsos_command_version

	mov di, command_clear_screen_text
	call salsos_compare_strings
	jc salsos_command_clear_screen

	mov di, command_help_text
	call salsos_compare_strings
	jc salsos_command_print_help

	mov di, command_shutdown_text
	call salsos_string_starts_with
	jc salsos_command_shutdown

	mov di, command_listdir_text
	call salsos_compare_strings
	jc salsos_command_listdir

	mov di, command_view_text
	call salsos_string_starts_with
	jc salsos_command_view

	mov di, command_run_text
	call salsos_string_starts_with
	jc salsos_command_run

	call salsos_command_unknown
	ret

salsos_command_run:
        ; The command is in si. We need to extract the filename from it
        mov ax, si
        add ax, 4
        mov si, ax

        ; si now holds the file. We can load it now
        mov ax, si
        mov cx, 32768

        call os_load_file

        jc .file_missing

	; Make sure the verification number is there
	mov si, 32768
	mov di, .exec_verif
	call salsos_compare_strings
	jnc .not_executable
  	
	mov ax, 0			; Clear all registers
	mov bx, 0
	mov cx, 0
	mov dx, 0
	mov word si, 0
	mov di, 0

	call 32808

        ret

	.exec_verif db 'This is an executable. Run it with run.', 0

.not_executable:
	mov si, .file_not_executable_text
	call salsos_print_string

	ret

	.file_not_executable_text db 'File not executable', 0

.file_missing:
        mov si, .file_missing_text
        call salsos_print_string

        ret

        .file_missing_text db 'Unknown file', 0

salsos_command_view:
	; Just make sure it doesn't have flags
	mov di, .s_flag
	call salsos_string_starts_with
	jc .view_size

	; The command is in si. We need to extract the filename from it
	mov ax, si
	add ax, 5
	mov si, ax

	; si now holds the file. We can load it now
	mov ax, si
	mov cx, .filecontents

	call os_load_file

	jc .file_missing

	; We have the file now!
	mov si, .filecontents
	call salsos_print_string

	ret

	.filetoview times 16 db 0
	.filecontents times 1024 db 0
	.s_flag db 'VIEW -S ', 0

.view_size:
	mov ax, si
	add ax, 8
	mov si, ax

	call os_get_file_size
	jc .file_missing
	mov ax, bx
	call salsos_int_to_string
	mov si, ax
	call salsos_print_string

	mov si, .byte
	call salsos_print_string

	ret

	.byte db ' bytes', 0

.file_missing:
	mov si, .file_missing_text
	call salsos_print_string

	ret

	.file_missing_text db 'Unknown file', 0

salsos_command_listdir:
	mov ax, .dirlist
	call os_get_file_list

	mov si, ax
	call salsos_print_string

	ret

	.dirlist times 256 db 0

salsos_command_shutdown:
	mov ax, si
	add ax, 9
	mov si, ax

	mov di, command_flag_s
	call salsos_compare_strings
	jc .shutdown

	mov di, command_flag_r
	call salsos_compare_strings
	jc .reboot

	jmp .none

.shutdown:
	mov ax, 0x1000
	mov ax, ss
	mov sp, 0xf000
	mov ax, 0x5307
	mov bx, 0x0001
	mov cx, 0x0003
	int 0x15

.reboot:
	mov ax, 0
	int 19h

.none:
        mov si, command_unknown_flag
        call salsos_print_string
	ret

salsos_command_print_help:
	mov si, command_help_message_title
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_clear
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_help
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_listdir
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_run
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_screentest
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_shutdown_a
	call salsos_print_string
	call salsos_newline
	mov si, command_help_message_shutdown_b
	call salsos_print_string
	call salsos_newline
	mov si, command_help_message_shutdown_c
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_version
	call salsos_print_string
	call salsos_newline

	mov si, command_help_message_view
	call salsos_print_string
	call salsos_newline
	mov si, command_help_message_view_a
	call salsos_print_string

	ret

salsos_command_clear_screen:
	; Clear the screen
	mov ah, 6			; Scroll full-screen
	mov al, 0			; Normal white on black
	mov bh, 7			;
	mov cx, 0			; Top-left
	mov dh, 24			; Bottom-right
	mov dl, 79
	int 10h

	; Put the cursor at the top left
	mov dx, 0
	call salsos_move_cursor

	ret

salsos_command_version:
	mov ax, [version_major]
        call salsos_print_number
        mov al, '.'
        call salsos_print_char
        mov ax, [version_minor]
        call salsos_print_number
        mov al, '.'
        call salsos_print_char
        mov ax, [version_patch]
        call salsos_print_number
	ret

salsos_command_screen_test:
	mov si, command_foo_bar
	call salsos_print_string
	ret

salsos_command_unknown:
	mov si, command_unknown
	call salsos_print_string

	ret

section .data:
	command_unknown db			'Unknown command', 0
	command_unknown_flag db			'Unknown flag', 0

	command_screen_test_text db		'SCREENTEST', 0
	command_show_version_text db		'VERSION', 0
	command_clear_screen_text db		'CLEAR', 0
	command_help_text db			'HELP', 0
	command_shutdown_text db		'SHUTDOWN', 0
	command_listdir_text db			'LISTDIR', 0
	command_view_text db			'VIEW', 0
	command_run_text db			'RUN', 0

	command_flag_s db			'-S', 0
	command_flag_r db			'-R', 0

	command_foo_bar db			'Foo Bar', 0
	command_help_message_title db		'Commands:', 0
	command_help_message_clear db		'clear: clears the screen', 0
	command_help_message_help db		'help: prints this message', 0
	command_help_message_listdir db		'listdir: lists all the files on the drive', 0
	command_help_message_view db		'view: prints the contents of the file following the command', 0
	command_help_message_view_a db		'      -s: prints the size of the file', 0
	command_help_message_run db		'run: runs an executable', 0
	command_help_message_screentest db	'screentest: prints a test message', 0
	command_help_message_shutdown_a db	'shutdown: shuts down the computer', 0
	command_help_message_shutdown_b db	'      -s: shutdown', 0
	command_help_message_shutdown_c db	'      -r: reboot', 0
	command_help_message_version db		'version: prints the OS version', 0
