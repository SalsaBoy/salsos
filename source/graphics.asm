	BITS 16

; Print String
; Prints a string the to screen
; SI—the string to print
salsos_print_string:
	pusha
	mov ah, 0Eh		; int 10h 'print char' function

.repeat:
	lodsb			; Get character from string
	cmp al, 0
	je .done		; If char is zero, end of string
	int 10h			; Otherwise, print it
	jmp .repeat

.done:
	popa
	ret

; Newline
; Prints a newline
salsos_newline:
	pusha

	mov ah, 0eh
	mov al, 0ah
	int 10h
	mov al, 0dh
	int 10h

	popa
	ret

; Print Char
; Prints only 1 character
; AL—character to print
salsos_print_char:
	pusha

	mov ah, 0eh
	int 10h

	popa
	ret

; Print number
; Converts a number to a char and prints it
; AX—The value to print
salsos_print_number:
	pusha

	cmp ax, 9
	jle .digit_format

	add ax, 'A'-'9'-1	; Correcting the values between 9 and A

.digit_format:
	add ax, '0'

	mov ah, 0eh
	int 10h

	popa
	ret

; Move Cursor
; Moves the cursor to the specified point on the screen
; DH—row number
; DL—column number
salsos_move_cursor:
	pusha

	mov bh, 0
	mov ah, 2
	int 10h

	popa
	ret
