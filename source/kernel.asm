
	BITS 16

	disk_buffer equ 24576

jmp start

	%INCLUDE "source/graphics.asm"
	%INCLUDE "source/input.asm"
	%INCLUDE "source/commands.asm"
	%INCLUDE "source/disk.asm"

start:
	mov ax, 2000h		; Set data segment to where we're loaded
	mov ds, ax

	; Show startup message
	mov si, welcome_text
	call salsos_print_string

	; Show the version number
	call salsos_command_version

	; Check for an autoex.run
	mov ax, .autoexec_name
	call os_file_exists
	jc mainloop
	call salsos_print_string

	mov si, .autoexec_command
	call salsos_check_command
	jmp mainloop

	.autoexec_name db 'AUTOEX.RUN', 0
	.autoexec_command db 'RUN AUTOEX.RUN', 0

mainloop:
	call salsos_newline
	call salsos_newline

	mov si, command_prompt
	call salsos_print_string

	mov si, command_input

.get_characters:
	call salsos_read_char
	call salsos_print_char

	cmp al, 0dh	; Is it enter
	je .return	; If so go to return script

	cmp al, 8h	; Is it backspace
	je .backspace

	cmp al, ' '	; Is it actually a character
	jl .get_characters

	mov [si], al
	inc si

	jmp .get_characters

.done:
	jmp mainloop

.backspace:
	; Just make sure we're not editing what we shouldn't
	cmp si, command_input
	jle .backspace_out_of_bounds

	sub si, 1
	mov [si], byte 0
	mov al, ' '
	call salsos_print_char
	mov al, 8h
	call salsos_print_char
	jmp .get_characters

.backspace_out_of_bounds:
	mov si, command_input
	jmp .get_characters

.return:
	mov al, 0ah
	call salsos_print_char

	; Command handling
	mov si, command_input
	call salsos_check_command

	; Reset values
	mov si, command_input

.reset_command:
	cmp [si], byte 0
	je .reset_end
	mov [si], byte 0
	inc si
	jmp .reset_command

.reset_end:
	jmp mainloop

; I'm just gonna put misc stuff here

; Fatal Error
; Prints an error message then haults the computer
; SI—The message
salsos_fatal_error:
	call salsos_print_string
	mov si, .reboot_pls
	call salsos_print_string

	jmp $

	.reboot_pls db	'Please reboot your computer', 0

; String Length
; Gets the length of a string
; AX—String location, returns length
salsos_string_length:
	pusha

	mov bx, ax			; Move location of string to BX

	mov cx, 0			; Counter

.more:
	cmp byte [bx], 0		; Zero (end of string) yet?
	je .done
	inc bx				; If not, keep adding
	inc cx
	jmp .more


.done:
	mov word [.tmp_counter], cx	; Store count before restoring other registers
	popa

	mov ax, [.tmp_counter]		; Put count back into AX before returning
	ret


	.tmp_counter	dw 0

; String Starts With
; Sees if one string starts with another
; SI—String
; DI—Prefix
; Flags—Returns the whether the string has the prefix
salsos_string_starts_with:
	pusha

.more:
	mov al, [si]
	mov bl, [di]

	cmp bl, 0
	je .prefix_ended

	cmp al, bl
	jne .different

	inc si
	inc di
	jmp .more

.different:
	popa
	clc
	ret

.prefix_ended:
	popa
	stc
	ret

; Uppercase String
; Converts a string to uppercase
; AX—String location
salsos_uppercase_string:
	pusha

	mov si, ax			; Use SI to access string

.more:
	cmp byte [si], 0		; Zero-termination of string?
	je .done			; If so, quit

	cmp byte [si], 'a'		; In the lower case A to Z range?
	jb .noatoz
	cmp byte [si], 'z'
	ja .noatoz

	sub byte [si], 20h		; If so, convert input char to upper case

	inc si
	jmp .more

.noatoz:
	inc si
	jmp .more

.done:
	popa
	ret

salsos_int_to_string:
	pusha

	mov cx, 0
	mov bx, 10			; Set BX 10, for division and mod
	mov di, .t			; Get our pointer ready

.push:
	mov dx, 0
	div bx				; Remainder in DX, quotient in AX
	inc cx				; Increase pop loop counter
	push dx				; Push remainder, so as to reverse order when popping
	test ax, ax			; Is quotient zero?
	jnz .push			; If not, loop again
.pop:
	pop dx				; Pop off values in reverse order, and add 48 to make them digits
	add dl, '0'			; And save them in the string, increasing the pointer each time
	mov [di], dl
	inc di
	dec cx
	jnz .pop

	mov byte [di], 0		; Zero-terminate string

	popa
	mov ax, .t			; Return location of string
	ret


	.t times 7 db 0

; Compare Strings
; Sees if 2 strings are equal
; SI—String 1
; DI—String 2
; Flags—Returns the equality of the strings
salsos_compare_strings:
	pusha

.more:
	mov al, [si]			; Retrieve string contents
	mov bl, [di]

	cmp al, bl			; Compare characters at current location
	jne .not_same

	cmp al, 0			; End of first string? Must also be end of second
	je .terminated

	inc si
	inc di
	jmp .more


.not_same:				; If unequal lengths with same beginning, the byte
	popa				; comparison fails at shortest string terminator
	clc				; Clear carry flag
	ret


.terminated:				; Both strings terminated at the same position
	popa
	stc				; Set carry flag
	ret

section .data:
	welcome_text db			'SalsOS ', 0
	version_major db		1, 0
	version_minor db		2, 0
	version_patch db		0, 0

	command_prompt db		'READY>', 0
	command_input db                0
