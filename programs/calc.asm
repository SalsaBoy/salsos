db 'This is an executable. Run it with run.', 0
jmp start

	%INCLUDE "source/salsos.inc"
	BITS 16
	ORG 32768

start:
	call clear
	call hide_cursor

	mov si, welcome
	call print

	mov si, calc_board
	call print

loop:
	call read_char
	cmp al, 27
	je end

	call read_char
	cmp al, 'l'
	je right

	jmp loop

right:
	inc cursor_x

	jmp loop

end:
	call show_cursor
	ret

welcome db 'CALC.RUN 1.0.0', NEWLINE, 0
calc_board 	db	218, 196, 196, 196, 196, 196, 196, 196, 196, 196, 191, NEWLINE
			db	179, '[',  49, ']',  32,  50,  32,  32,  51,  32, 179, NEWLINE
            db  179,  32,  52,  32,  32,  53,  32,  32,  54,  32, 179, NEWLINE
            db  179,  32,  55,  32,  32,  56,  32,  32,  57,  32, 179, NEWLINE
			db	192, 196, 196, 196, 196, 196, 196, 196, 196, 196, 217, NEWLINE, 0

cursor_x	db 0
cursor_y	db 0
