nasm boot.asm -f bin -o boot.bin
nasm source/kernel.asm -f bin -o kernel.bin

rm salsos.img
cp blank.img salsos.img
dd conv=notrunc if=boot.bin of=salsos.img
